﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Pay.Entity
*文件名： RedPackEnum
*创建人： kingling
*创建时间：2018/11/16 12:35:02
*描述
*=======================================================================
*修改标记
*修改时间：2018/11/16 12:35:02
*修改人：kingling
*描述：
************************************************************************/

namespace Wx.Sdk.Pay.Entity
{
    public enum RedPackEnum
    {
        /// <summary>
        /// 商品促销
        /// </summary>
        PRODUCT_1 = 0,
        /// <summary>
        /// 抽奖
        /// </summary>
        PRODUCT_2,
        /// <summary>
        /// 虚拟物品兑奖 
        /// </summary>
        PRODUCT_3,
        /// <summary>
        /// 企业内部福利
        /// </summary>
        PRODUCT_4,
        /// <summary>
        /// 渠道分润
        /// </summary>
        PRODUCT_5,
        /// <summary>
        /// 保险回馈
        /// </summary>
        PRODUCT_6,
        /// <summary>
        /// 彩票派奖
        /// </summary>
        PRODUCT_7,
        /// 税务刮奖
        PRODUCT_8,
    }
}
