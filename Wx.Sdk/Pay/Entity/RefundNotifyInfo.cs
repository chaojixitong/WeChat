﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Pay.Entity
*文件名： RefundNotifyInfo
*创建人： kingling
*创建时间：2018/10/6 17:23:58
*描述
*=======================================================================
*修改标记
*修改时间：2018/10/6 17:23:58
*修改人：kingling
*描述：
************************************************************************/

namespace Wx.Sdk.Pay.Entity
{
    /// <summary>
    /// 退款通知数据解密
    /// </summary>
    public class RefundNotifyInfo
    {
        /// <summary>
        /// 微信订单号
        /// </summary>
        public string transaction_id { get; set; }
        /// <summary>
        /// 商户订单号
        /// </summary>
        public string out_trade_no { get; set; }
        /// <summary>
        /// 微信退款单号
        /// </summary>
        public string refund_id { get; set; }
        /// <summary>
        /// 商户退款单号
        /// </summary>
        public string out_refund_no { get; set; }
        /// <summary>
        /// 订单金额
        /// 单位为分
        /// </summary>
        public int total_fee { get; set; }
        /// <summary>
        /// 应结订单金额
        /// 当该订单有使用非充值券时，返回此字段。应结订单金额=订单金额-非充值代金券金额，应结订单金额<=订单金额。
        /// 单位为分
        /// </summary>
        public int settlement_total_fee { get; set; }
        /// <summary>
        /// 申请退款金额
        /// 单位为分
        /// </summary>
        public int refund_fee { get; set; }
        /// <summary>
        /// 退款金额
        /// 退款金额=申请退款金额-非充值代金券退款金额，退款金额<=申请退款金额
        /// </summary>
        public int settlement_refund_fee { get; set; }
        /// <summary>
        /// 退款状态
        /// SUCCESS-退款成功
        /// CHANGE-退款异常
        /// REFUNDCLOSE—退款关闭
        /// </summary>
        public string refund_status { get; set; }
        /// <summary>
        /// 退款成功时间
        /// 资金退款至用户帐号的时间，格式2017-12-15 09:46:01
        /// </summary>
        public string success_time { get; set; }
        /// <summary>
        /// 退款入账账户
        /// </summary>
        public string refund_recv_accout { get; set; }
        /// <summary>
        /// 退款资金来源
        /// REFUND_SOURCE_RECHARGE_FUNDS 可用余额退款/基本账户
        /// REFUND_SOURCE_UNSETTLED_FUNDS 未结算资金退款
        /// </summary>
        public string refund_account { get; set; }
        /// <summary>
        /// 退款发起来源
        /// API接口
        /// VENDOR_PLATFORM商户平台
        /// </summary>
        public string refund_request_source { get; set; }
    }
}
