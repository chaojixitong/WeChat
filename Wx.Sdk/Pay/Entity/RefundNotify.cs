﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Pay.Entity
*文件名： RefundResult
*创建人： kingling
*创建时间：2018/10/6 17:21:05
*描述
*=======================================================================
*修改标记
*修改时间：2018/10/6 17:21:05
*修改人：kingling
*描述：
************************************************************************/

namespace Wx.Sdk.Pay.Entity
{
    /// <summary>
    /// 退款通知数据
    /// </summary>
    public class RefundNotify
    {
        /// <summary>
        /// 返回状态码 SUCCESS/FAIL 此字段是通信标识，非交易标识，交易是否成功需要查看result_code来判断
        /// </summary>
        public string return_code { get; set; }
        /// <summary>
        /// 返回信息，如非空，为错误原因
        /// </summary>
        public string return_msg { get; set; }
        //--------------以下字段在return_code为SUCCESS的时候有返回
        /// <summary>
        /// 公众账号ID 微信分配的公众账号ID（企业号corpid即为此appId）
        /// </summary>
        public string appid { get; set; }
        /// <summary>
        /// 商户号
        /// </summary>
        public string mch_id { get; set; }
        /// <summary>
        /// 随机字符串 随机字符串，不长于32位
        /// </summary>
        public string nonce_str { get; set; }
        /// <summary>
        /// 加密信息请用商户秘钥进行解密
        /// （1）对加密串A做base64解码，得到加密串B
        /// （2）对商户key做md5，得到32位小写key* ( key设置路径：微信商户平台(pay.weixin.qq.com)-->账户设置-->API安全-->密钥设置 )
        /// （3）用key*对加密串B做AES-256-ECB解密（PKCS7Padding）
        /// </summary>
        public string req_info { get; set; }
    }
}
