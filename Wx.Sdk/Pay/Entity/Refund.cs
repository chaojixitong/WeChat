﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Pay.Entity
*文件名： Refund
*创建人： kingling
*创建时间：2018/10/6 17:07:44
*描述
*=======================================================================
*修改标记
*修改时间：2018/10/6 17:07:44
*修改人：kingling
*描述：
************************************************************************/

namespace Wx.Sdk.Pay.Entity
{
    /// <summary>
    /// 退款提交数据
    /// </summary>
    public class Refund
    {
        public Refund()
        {
            this.refund_fee_type = "CNY";
        }
        /// <summary>
        /// 公众账号ID,微信支付分配的公众账号ID（企业号corpid即为此appId）
        /// </summary>
        public string appid { get; set; }
        /// <summary>
        /// 微信支付分配的商户号
        /// </summary>
        public string mch_id { get; set; }
        /// <summary>
        /// 随机字符串，不长于32位
        /// </summary>
        public string nonce_str { get; set; }
        /// <summary>
        /// 签名
        /// </summary>
        public string sign { get; set; }
        /// <summary>
        /// 签名类型，目前支持HMAC-SHA256和MD5，默认为MD5
        /// </summary>
        public string sign_type { get; set; }
        /// <summary>
        /// 商户系统内部订单号，要求32个字符内，只能是数字、大小写字母_-|*@ ，且在同一个商户号下唯一。
        /// </summary>
        public string out_trade_no { get; set; }
        /// <summary>
        /// 商户系统内部的退款单号，商户系统内部唯一，只能是数字、大小写字母_-|*@ ，同一退款单号多次请求只退一笔。
        /// </summary>
        public string out_refund_no { get; set; }
        /// <summary>
        /// 订单总金额，单位为分，只能为整数
        /// </summary>
        public int total_fee { get; set; }
        /// <summary>
        /// 退款总金额，订单总金额，单位为分，只能为整数
        /// </summary>
        public int refund_fee { get; set; }
        /// <summary>
        /// 退款货币类型，需与支付一致，或者不填。符合ISO 4217标准的三位字母代码，默认人民币：CNY
        /// </summary>
        public string refund_fee_type { get; set; }
        /// <summary>
        /// 若商户传入，会在下发给用户的退款消息中体现退款原因
        /// </summary>
        public string refund_desc { get; set; }
        /// <summary>
        /// 异步接收微信支付退款结果通知的回调地址，通知URL必须为外网可访问的url，不允许带参数
        /// 如果参数中传了notify_url，则商户平台上配置的回调地址将不会生效。
        /// </summary>
        public string notify_url { get; set; }
    }
}
