﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Pay.Entity
*文件名： AppPayInfo
*创建人： kingling
*创建时间：2018/10/6 17:06:24
*描述
*=======================================================================
*修改标记
*修改时间：2018/10/6 17:06:24
*修改人：kingling
*描述：
************************************************************************/


namespace Wx.Sdk.Pay.Entity
{
    /// <summary>
    /// APP支付需要的信息
    /// </summary>
    public class AppPayInfo
    {
        public AppPayInfo()
        {
            this.signType = "MD5";
            this.package = "Sign=WXPay";
        }
        /// <summary>
        /// 公众号ID 商户注册具有支付权限的公众号成功后即可获得
        /// </summary>
        public string appid { get; set; }
        /// <summary>
        /// 时间戳
        /// </summary>
        public string timestamp { get; set; }
        /// <summary>
        /// 随机字符串，不长于32位
        /// </summary>
        public string noncestr { get; set; }
        /// <summary>
        /// 统一下单接口返回的prepay_id参数值，提交格式如：prepay_id=***
        /// </summary>
        public string package { get; set; }
        /// <summary>
        /// 签名算法，默认MD5
        /// </summary>
        public string signType { get; set; }
        /// <summary>
        /// 签名
        /// </summary>
        public string sign { get; set; }
        /// <summary>
        /// 商户号
        /// </summary>
        public string partnerid { get; set; }
        /// <summary>
        /// 预支付交易会话ID
        /// </summary>
        public string prepayid { get; set; }
    }
}
