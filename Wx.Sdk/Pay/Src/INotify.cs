﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Pay.Src
*文件名： IPayNotify
*创建人： kingling
*创建时间：2018/10/18 13:55:18
*描述
*=======================================================================
*修改标记
*修改时间：2018/10/18 13:55:18
*修改人：kingling
*描述：
************************************************************************/


namespace Wx.Sdk.Pay.Src
{
    public interface INotify
    {
        /// <summary>
        /// 退款通知
        /// </summary>
        /// <returns></returns>
        string Refund();
        /// <summary>
        /// 支付通知
        /// </summary>
        string Pay();
    }
}
