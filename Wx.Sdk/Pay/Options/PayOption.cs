﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Pay.Options
*文件名： PayOption
*创建人： kingling
*创建时间：2018/10/6 17:55:39
*描述
*=======================================================================
*修改标记
*修改时间：2018/10/6 17:55:39
*修改人：kingling
*描述：
************************************************************************/

namespace Wx.Sdk.Pay.Options
{
    public class PayOption
    {
        public PayOption()
        {
            this.sign_type = "MD5";
        }
        /// <summary>
        /// 支付账户的微信ID 关联服务号的APPID
        /// </summary>
        public string appid { get; set; }
        /// <summary>
        /// 微信支付商户号
        /// </summary>
        public string mchid { get; set; }
        /// <summary>
        /// 签名算法，默认为MD5，支持HMAC-SHA256和MD5。
        /// </summary>
        public string sign_type { get; set; }
        /// <summary>
        /// 加密密钥
        /// </summary>
        public string sign_key { get; set; }
        /// <summary>
        /// SSL证书目录 相对路径（仅退款、撤销订单时需要）
        /// </summary>
        public string sslpath { get; set; }
        /// <summary>
        /// 证书密码
        /// </summary>
        public string sslpassword { get; set; }
        /// <summary>
        /// 支付异步消息回调
        /// </summary>
        public string pay_notify { get; set; }
        /// <summary>
        /// 退款异步通知地址
        /// </summary>
        public string refund_notify { get; set; }
        /// <summary>
        /// 通知域名
        /// </summary>
        public string notify_url { get; set; }
    }
}
