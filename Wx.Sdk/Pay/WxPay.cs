﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Pay
*文件名： WxPay
*创建人： kingling
*创建时间：2018/10/6 17:52:52
*描述
*=======================================================================
*修改标记
*修改时间：2018/10/6 17:52:52
*修改人：kingling
*描述：
************************************************************************/

using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System.Net.Http;
using Wx.Sdk.Pay.Base;
using Wx.Sdk.Pay.Options;
using Wx.Sdk.Pay.Src;

namespace Wx.Sdk.Pay
{
    public sealed class WxPay
    {
        internal PayOption payOption;
        internal IHttpClientFactory httpClientFactory;
        internal IHttpContextAccessor httpContextAccessor;
        internal PayBase payBase;
        IPayOrder _payOrder;
        public WxPay(IHttpContextAccessor httpContextAccessor, IOptions<PayOption> options, IHttpClientFactory httpClientFactory)
        {
            payOption = options.Value;
            this.httpContextAccessor = httpContextAccessor;
            this.httpClientFactory = httpClientFactory;
            payBase = new PayBase(this);
        }
        private WxPay()
        {
        }
        public IPayOrder PayOrder
        {
            get
            {
                if (_payOrder == null)
                {
                    this._payOrder = new PayOrder(this);
                }
                return _payOrder;
            }
        }
    }
}
