﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Utils
*文件名： TimeHelper
*创建人： kingling
*创建时间：2018/9/29 19:23:30
*描述
*=======================================================================
*修改标记
*修改时间：2018/9/29 19:23:30
*修改人：kingling
*描述：
************************************************************************/

using System;

namespace Wx.Sdk.Utils
{
    internal static class TimeHelper
    {
        /// <summary>
        /// unix时间戳
        /// </summary>
        /// <returns></returns>
        public static int ToUnix() {
            return (int)((DateTime.Now.ToUniversalTime().Ticks - 621355968000000000) / 10000000);
        }
    }
}
