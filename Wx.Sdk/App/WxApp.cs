﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using Wx.Sdk.App.Base;
using Wx.Sdk.App.Options;
using Wx.Sdk.App.Src;

namespace Wx.Sdk.App
{
    public sealed class WxApp
    {
        internal AppOption appOption;
        internal IHttpClientFactory httpClientFactory;
        internal AppBase appBase;
        IAppLogin _appLogin;
        public WxApp(IOptions<AppOption> options, IHttpClientFactory httpClientFactory)
        {
            appOption = options.Value;
            this.httpClientFactory = httpClientFactory;
            appBase = new AppBase(this);
        }
        private WxApp()
        {
        }
        public IAppLogin AppLogin
        {
            get
            {
                if (_appLogin == null)
                {
                    this._appLogin = new AppLogin(this);
                }
                return _appLogin;
            }
        }
    }
}
