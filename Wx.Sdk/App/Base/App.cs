﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Wx.Sdk.App;
using Wx.Sdk.App.Options;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class App
    {
        /// <summary>
        /// 小程序注入
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public static IServiceCollection AddWeChatApp(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddHttpClient();
            services.TryAddSingleton<WxApp>();
            services.Configure<AppOption>(configuration);
            return services;
        }
    }
}
