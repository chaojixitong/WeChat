﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Wx.Sdk.Utils;

namespace Wx.Sdk.App.Base
{
    internal class AppBase
    {
        WxApp wxApp;
        internal AppBase(WxApp wxApp)
        {
            this.wxApp = wxApp;
        }
        /// <summary>
        /// GET数据
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public async Task<string> Get(string url)
        {
            Contract.Assume(!url.IsNull());
            var client = wxApp.httpClientFactory.CreateClient();
            //远程拉取字符串
            var data = await client.GetStringAsync(url);
            return data;
        }
        /// <summary>
        /// POST数据
        /// </summary>
        /// <param name="body"></param>
        /// <param name="url"></param>
        /// <returns></returns>
        public async Task<string> Post(string body, string url)
        {
            Contract.Assume(!url.IsNull());
            var client = wxApp.httpClientFactory.CreateClient();
            var content = new StringContent(body);
            var responseMsg = await client.PostAsync(url, content);
            var data = await responseMsg.Content.ReadAsStringAsync();
            return data;
        }
    }
}
