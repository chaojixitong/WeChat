﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wx.Sdk.App.Options
{
    public class AppOption
    {
        /// <summary>
        /// 小程序appid
        /// </summary>
        public string appid { get; set; }
        /// <summary>
        /// 小程序secret
        /// </summary>
        public string secret { get; set; }
    }
}
