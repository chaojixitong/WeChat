﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wx.Sdk.Ext
{
    public class WxError : ApplicationException
    {
        private WxError()
        {

        }
        public WxError(string message) : base(message)
        {

        }
        public WxError(string message, WxError innerWxError) : base(message, innerWxError)
        {

        }
    }
}
