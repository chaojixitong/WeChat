﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Api.Messages.Base
*文件名： MsgBase
*创建人： kingling
*创建时间：2018/9/29 15:02:06
*描述
*=======================================================================
*修改标记
*修改时间：2018/9/29 15:02:06
*修改人：kingling
*描述：
************************************************************************/

namespace Wx.Sdk.Api.Entity.Base
{
    public class MsgBase:Msg
    {
        public ulong MsgId { get; set; }
    }
}
