﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Api.Messages
*文件名： UnSubscribeEvent
*创建人： kingling
*创建时间：2018/9/29 15:28:22
*描述
*=======================================================================
*修改标记
*修改时间：2018/9/29 15:28:22
*修改人：kingling
*描述：
************************************************************************/

using Wx.Sdk.Api.Entity.Base;

namespace Wx.Sdk.Api.Entity.Messages
{
    /// <summary>
    /// 取消关注 Event=unsubscribe
    /// </summary>
    public class UnSubscribeEvent:EventBase
    {
    }
}
