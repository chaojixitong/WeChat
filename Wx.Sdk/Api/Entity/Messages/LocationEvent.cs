﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Api.Messages
*文件名： LocationEvent
*创建人： kingling
*创建时间：2018/9/29 15:32:23
*描述
*=======================================================================
*修改标记
*修改时间：2018/9/29 15:32:23
*修改人：kingling
*描述：
************************************************************************/

using Wx.Sdk.Api.Entity.Base;

namespace Wx.Sdk.Api.Entity.Messages
{
    /// <summary>
    /// 上报地理位置事件,事件类型，LOCATION
    /// </summary>
    public class LocationEvent: EventBase
    {
        /// <summary>
        /// 地理位置维度
        /// </summary>
        public decimal Latitude { get; set; }
        /// <summary>
        /// 地理位置经度
        /// </summary>
        public decimal Longitude { get; set; }
        /// <summary>
        /// 地理位置精度
        /// </summary>
        public decimal Precision { get; set; }
    }
}
