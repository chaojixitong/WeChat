﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Api.Messages
*文件名： VideoMsg
*创建人： kingling
*创建时间：2018/9/29 15:21:33
*描述
*=======================================================================
*修改标记
*修改时间：2018/9/29 15:21:33
*修改人：kingling
*描述：
************************************************************************/

using Wx.Sdk.Api.Entity.Base;

namespace Wx.Sdk.Api.Entity.Messages
{
    /// <summary>
    /// 接收视频消息，包含小视频消息 MsgType=video/ MsgType=shortvideo
    /// </summary>
    public class VideoMsg : MsgBase
    {
        /// <summary>
        /// 视频消息媒体id，可以调用多媒体文件下载接口拉取数据。
        /// </summary>
        public string MediaId { get; set; }
        /// <summary>
        /// 视频消息缩略图的媒体id，可以调用多媒体文件下载接口拉取数据。
        /// </summary>
        public string ThumbMediaId { get; set; }
    }
}
