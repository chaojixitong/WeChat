﻿
namespace Wx.Sdk.Api.Entity
{
    internal class WxMsg
    {
        public int errcode { set; get; }
        public string errmsg { set; get; }
        public string msgid { get; set; }
    }
}
