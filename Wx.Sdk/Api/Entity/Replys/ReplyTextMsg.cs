﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Api.Messages.Replys
*文件名： ReplyTextMsg
*创建人： kingling
*创建时间：2018/9/29 18:58:57
*描述
*=======================================================================
*修改标记
*修改时间：2018/9/29 18:58:57
*修改人：kingling
*描述：
************************************************************************/

using System.Xml.Serialization;
using Wx.Sdk.Api.Entity.Base;

namespace Wx.Sdk.Api.Entity.Replys
{
    /// <summary>
    /// 回复文字消息
    /// </summary>
    internal class ReplyTextMsg : Msg
    {
        public ReplyTextMsg()
        {
            MsgType = "text";
        }
        /// <summary>
        /// 文字内容
        /// </summary>
        public string Content { get; set; }
    }
}
