﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Api.Messages.Replys
*文件名： ReplyMusicMsg
*创建人： kingling
*创建时间：2018/9/29 19:39:26
*描述
*=======================================================================
*修改标记
*修改时间：2018/9/29 19:39:26
*修改人：kingling
*描述：
************************************************************************/

using System.Xml.Serialization;
using Wx.Sdk.Api.Entity.Base;

namespace Wx.Sdk.Api.Entity.Replys
{
    /// <summary>
    /// 回复音乐消息
    /// </summary>
    internal class ReplyMusicMsg : Msg
    {
        public ReplyMusicMsg()
        {
            MsgType = "music";
        }
        public Music Music { get; set; }
    }
}
