﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Api.Messages.Replys
*文件名： Voice
*创建人： kingling
*创建时间：2018/9/29 19:54:39
*描述
*=======================================================================
*修改标记
*修改时间：2018/9/29 19:54:39
*修改人：kingling
*描述：
************************************************************************/

namespace Wx.Sdk.Api.Entity.Replys
{
    /// <summary>
    /// 语音实体
    /// </summary>
    public class Voice
    {
        /// <summary>
        /// 通过上传多媒体文件，得到的id。
        /// </summary>
        public string MediaId { get; set; }
    }
}
