﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Api.Messages.Replys
*文件名： Music
*创建人： kingling
*创建时间：2018/9/29 19:39:41
*描述
*=======================================================================
*修改标记
*修改时间：2018/9/29 19:39:41
*修改人：kingling
*描述：
************************************************************************/

namespace Wx.Sdk.Api.Entity.Replys
{
    /// <summary>
    /// 音乐消息
    /// </summary>
    public class Music
    {
        /// <summary>
        /// 音乐消息的标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 音乐消息的描述
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// 音乐链接
        /// </summary>
        public string MusicURL { get; set; }
        /// <summary>
        /// 高质量音乐链接，WIFI环境优先使用该链接播放音乐
        /// </summary>
        public string HQMusicUrl { get; set; }
        /// <summary>
        /// 缩略图的媒体id，通过上传多媒体文件，得到的id
        /// </summary>
        public string ThumbMediaId { get; set; }
    }
}
