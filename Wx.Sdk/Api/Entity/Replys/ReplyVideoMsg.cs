﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Api.Messages.Replys
*文件名： ReplyVideoMsg
*创建人： kingling
*创建时间：2018/9/29 19:56:43
*描述
*=======================================================================
*修改标记
*修改时间：2018/9/29 19:56:43
*修改人：kingling
*描述：
************************************************************************/

using System.Xml.Serialization;
using Wx.Sdk.Api.Entity.Base;

namespace Wx.Sdk.Api.Entity.Replys
{
    /// <summary>
    /// 回复视频消息
    /// </summary>
    internal class ReplyVideoMsg:Msg
    {
        public ReplyVideoMsg()
        {
            MsgType = "video";
        }
        public Video Video { get; set; }
    }
}
