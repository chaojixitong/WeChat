﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Api.Messages.Replys
*文件名： ReplyNewsMsg
*创建人： kingling
*创建时间：2018/9/29 19:13:58
*描述
*=======================================================================
*修改标记
*修改时间：2018/9/29 19:13:58
*修改人：kingling
*描述：
************************************************************************/

using System.Collections.Generic;
using System.Xml.Serialization;
using Wx.Sdk.Api.Entity.Base;

namespace Wx.Sdk.Api.Entity.Replys
{
    /// <summary>
    /// 回复图文消息
    /// </summary>
    internal class ReplyNewsMsg : Msg
    {
        public ReplyNewsMsg()
        {
            MsgType = "news";
        }
        /// <summary>
        /// 图文消息个数，限制为8条以内
        /// </summary>
        public int ArticleCount { get; set; }
        /// <summary>
        /// 富文本
        /// </summary>
        [XmlArrayItem("item")]
        public IList<Article> Articles { get; set; }
    }
}
