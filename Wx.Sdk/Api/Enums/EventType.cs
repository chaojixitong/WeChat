﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Api.Enums
*文件名： EventType
*创建人： kingling
*创建时间：2018/9/29 13:04:47
*描述
*=======================================================================
*修改标记
*修改时间：2018/9/29 13:04:47
*修改人：kingling
*描述：
************************************************************************/

namespace Wx.Sdk.Api.Enums
{
    /// <summary>
    /// 事件消息类型
    /// </summary>
    internal enum EventType
    {
        /// <summary>
        /// 关注事件
        /// </summary>
        subscribe = 0,

        /// <summary>
        /// 取消关注事件
        /// </summary>
        unsubscribe,

        /// <summary>
        /// 扫描带参数二维码事件,用户已关注时的事件推送
        /// </summary>
        SCAN,

        /// <summary>
        /// 上报地理位置
        /// </summary>
        LOCATION,

        /// <summary>
        /// 自定义菜单事件
        /// </summary>
        CLICK,

        /// <summary>
        /// 点击菜单跳转链接时的事件推送
        /// </summary>
        VIEW,
        /// <summary>
        /// 模板消息推送返回消息
        /// </summary>
        TEMPLATESENDJOBFINISH
    }
}
