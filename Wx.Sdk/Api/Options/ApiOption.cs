﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Api.Options
*文件名： ApiOption
*创建人： kingling
*创建时间：2018/9/29 20:06:13
*描述
*=======================================================================
*修改标记
*修改时间：2018/9/29 20:06:13
*修改人：kingling
*描述：
************************************************************************/

namespace Wx.Sdk.Api.Options
{
    public class ApiOption
    {
        /// <summary>
        /// 应用ID
        /// </summary>
        public string appid { get; set; }
        /// <summary>
        /// 应用密钥
        /// </summary>
        public string appsecret { get; set; }
        /// <summary>
        /// 令牌
        /// </summary>
        public string token { get; set; }
        /// <summary>
        /// 消息加解密密钥
        /// </summary>
        public string encodingaeskey { get; set; }
        /// <summary>
        /// 接收消息地址
        /// </summary>
        public string url { get; set; }
    }
}
