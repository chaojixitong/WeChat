﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wx.Sdk.Api.Base
{
    public interface IWxCache
    {
        /// <summary>
        /// 添加缓存
        /// </summary>
        /// <param name="key"></param>
        /// <param name="content"></param>
        /// <param name="time">相对过期时间,秒</param>
        /// <returns></returns>
        void Set(string key, object content, int time = 7200);
        /// <summary>
        /// 获取缓存
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        T Get<T>(string key);
    }
}
