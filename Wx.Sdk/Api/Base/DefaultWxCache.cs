﻿using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Text;

namespace Wx.Sdk.Api.Base
{
    internal class DefaultWxCache : IWxCache
    {
        private IMemoryCache _cache;
        public DefaultWxCache(IMemoryCache cache)
        {
            _cache = cache;
        }
        T IWxCache.Get<T>(string key)
        {
            Contract.Assume(!key.IsNull());
            return _cache.Get<T>(key);
        }

        void IWxCache.Set(string key, object content, int time)
        {
            Contract.Assume(!key.IsNull() && content != null);
            _cache.Set(key, content, TimeSpan.FromSeconds(time));
        }
    }
}
