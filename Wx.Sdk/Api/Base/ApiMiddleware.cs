﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Api
*文件名： Midware
*创建人： kingling
*创建时间：2018/9/29 20:57:54
*描述
*=======================================================================
*修改标记
*修改时间：2018/9/29 20:57:54
*修改人：kingling
*描述：
************************************************************************/

using Microsoft.AspNetCore.Http;
using System;
using System.Threading.Tasks;
using Wx.Sdk.Api.Src;

namespace Wx.Sdk.Api.Base
{
    public class ApiMiddleware
    {
        private RequestDelegate next = null;
        public ApiMiddleware(RequestDelegate next)
        {
            this.next = next;
        }
        public async Task Invoke(HttpContext context, IWxMessage message, WxApi wxApi)
        {
            if (context.Request.Path.Value == wxApi.apiOption.url && message != null)
            {

                try
                {
                    var msg = await message.HaveMsg();
                    await context.Response.WriteAsync(msg);
                }
                catch {}
                return;
            }
            if (next != null) await next.Invoke(context);
        }
    }
}
