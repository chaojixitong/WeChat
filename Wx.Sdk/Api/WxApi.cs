﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Api
*文件名： WeChat
*创建人： kingling
*创建时间：2018/6/29 18:08:00
*描述
*=======================================================================
*修改标记
*修改时间：2018/6/29 18:08:00
*修改人：kingling
*描述：
************************************************************************/

using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System.Net.Http;
using Wx.Sdk.Api.Base;
using Wx.Sdk.Api.Options;
using Wx.Sdk.Api.Src;

namespace Wx.Sdk.Api
{
    public sealed class WxApi
    {
        internal ApiOption apiOption;
        internal IHttpContextAccessor httpContextAccessor;
        internal ApiBase apiBase;
        internal IHttpClientFactory httpClientFactory;
        internal IWxCache wxCache;
        IApiService _ApiService = null;
        public WxApi(IHttpContextAccessor httpContextAccessor, IOptions<ApiOption> options, IHttpClientFactory httpClientFactory, IWxCache wxCache)
        {
            this.apiOption = options.Value;
            this.httpContextAccessor = httpContextAccessor;
            this.httpClientFactory = httpClientFactory;
            this.wxCache = wxCache;
            apiBase = new ApiBase(this);
        }
        private WxApi()
        {
        }
        /// <summary>
        /// 微信其他API
        /// </summary>
        public IApiService ApiService
        {
            get
            {
                if (_ApiService == null)
                {
                    this._ApiService = new ApiService(this);
                }
                return _ApiService;
            }
        }
    }
}
