﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Wx.Sdk.Api;
using Wx.Sdk.Api.Entity.Template;
using Wx.Sdk.Pay;

namespace Web.Core.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return Content(DateTime.Now.Hour.ToString());
        }
        WxApi wxApi;
        WxPay wxPay;
        public HomeController(WxApi wxApi, WxPay wxPay)
        {
            this.wxApi = wxApi;
            this.wxPay = wxPay;
        }
        public string TestSendTemplateMsg()
        {
            wxApi.ApiService.SendTemplateMsg("osg8b1TVkLUH6O1MEraM0geglP4g", "AMv8Yqpx0cioJkB24pIjNtt2gRS2kluidvLuegMuohw",
            new Keyword("标题"), new Keyword("备注"),
            "", null,
            new Keyword("1"),
            new Keyword("1"),
            new Keyword("1"));
            return "1";
        }
        public async Task<IActionResult> Pay()

        {
            var no = Guid.NewGuid().ToString("N");
            //var a = await wxPay.payOrder.JsPay(0.01m, no, "aaa", "orCWv0fpsdD9MZ1TPuhihBVEEtfY");
            var url = await wxPay.payOrder.QRCodePay(0.01m, no, "test", "1");
            //return Redirect(url);
            return Content(url);
        }
        public async Task<bool> Refund(string no)
        {
            var no1 = Guid.NewGuid().ToString("N");
            return await wxPay.payOrder.Refund(no1, no, 0.01m, 0.01m);
        }
        public async Task<bool> Payment()
        {
            var no1 = Guid.NewGuid().ToString("N");
            return await wxPay.payOrder.Payment(no1, "oSHqY1HQoQ76afjuA1qGCU4rqgA4", 1, "付款",false);
        }
        public async Task<bool> Red()
        {
            var no1 = Guid.NewGuid().ToString("N").Substring(0,27);
            return await wxPay.payOrder.NormalRedPark(no1, "oSHqY1HQoQ76afjuA1qGCU4rqgA4", 1, "大哥", "拉阿鲁","生日快乐","快点领取吧",Wx.Sdk.Pay.Entity.RedPackEnum.PRODUCT_2);
        }
        public async Task<IActionResult> t()
        {
            return Content("a");
        }
    }
}